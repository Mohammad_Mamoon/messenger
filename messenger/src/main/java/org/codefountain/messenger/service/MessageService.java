package org.codefountain.messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codefountain.messenger.database.DatabaseClass;
import org.codefountain.messenger.exception.DataNotFoundException;
import org.codefountain.messenger.model.Message;

public class MessageService {

	private Map<Long, Message> messages = DatabaseClass.getMessages();
	
	public MessageService() {
		messages.put(1l, new Message(1,"Hello World",new Date(),"Mamoon"));
		messages.put(2l, new Message(2,"Jersey",new Date(),"Mamoon"));
	}

	public List<Message> getMessages() {
		return new ArrayList<Message>(messages.values());

	}
	
	public List<Message> getMessagesForYear(int year) {
		ArrayList<Message> list = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		for(Message m: messages.values()) {
			calendar.setTime(m.getCreated());
			if(calendar.get(Calendar.YEAR) == year)
				list.add(m);
		}
		
		return list;
	}
	
	public List<Message> getMessagesPaginated(int start, int size) {
		ArrayList<Message> list = new ArrayList<>(messages.values());
		if(start + size > list.size())
			return new ArrayList<Message>();
		
		return list.subList(start, start + size);
	}

	public Message getMessage(long id) {
		Message message =  messages.get(id);
		if(message == null) {
			throw new DataNotFoundException("Message with id " + id + " not found");
		} else {
			return message;
		}
	}

	public Message addMessage(Message message) {
		message.setId(messages.size() + 1);
		messages.put(message.getId(), message);
		return message;
	}

	public Message updateMessage(Message message) {
		if (message.getId() <= 0) {
			return null;
		}

		messages.put(message.getId(), message);
		return message;
	}

	public Message removeMessage(long id) {
		return messages.remove(id);
	}

}

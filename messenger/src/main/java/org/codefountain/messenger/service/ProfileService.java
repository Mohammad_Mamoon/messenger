package org.codefountain.messenger.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.codefountain.messenger.database.DatabaseClass;
import org.codefountain.messenger.model.Profile;

public class ProfileService {
	
	private Map<String, Profile> profiles = DatabaseClass.getProfiles();
	
	public ProfileService() {
		profiles.put("Mamoon", new Profile(1l,"Mamoon","Mamoon","Roshangar"));
		profiles.put("Tabish", new Profile(2l,"Tabish","Tabish","snail"));
	}
	
	
	public List<Profile> getProfiles() {
		return new ArrayList<Profile>(profiles.values());

	}

	public Profile getProfile(String name) {
		return profiles.get(name);
	}

	public Profile addProfile(Profile profile) {
		profile.setId(profiles.size() + 1);
		profiles.put(profile.getProfileName(), profile);
		return profile;
	}

	public Profile updateProfile(Profile profile) {
		if (profile.getProfileName().isEmpty()) {
			return null;
		}

		profiles.put(profile.getProfileName(), profile);
		return profile;
	}

	public Profile removeProfile(String name) {
		return profiles.remove(name);
	}

}

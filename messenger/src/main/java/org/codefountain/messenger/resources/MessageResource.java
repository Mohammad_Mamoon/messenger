package org.codefountain.messenger.resources;

import java.net.URI;
import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.codefountain.messenger.model.Message;
import org.codefountain.messenger.model.MessageFilterBean;
import org.codefountain.messenger.service.MessageService;

@Path("/messages")
public class MessageResource {

	MessageService messageService = new MessageService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Message> getMessages(@BeanParam MessageFilterBean filterBean) {
		if(filterBean.getYear() > 0)
			return messageService.getMessagesForYear(filterBean.getYear());
		
		if(filterBean.getStart() > 0  && filterBean.getSize() > 0)
			return messageService.getMessagesPaginated(filterBean.getStart(), filterBean.getSize());
		
		return messageService.getMessages();
	}

	@GET
	@Path("/message/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMessage(@PathParam("id") long id, @Context UriInfo uriInfo) {
		Message message = messageService.getMessage(id);
		message.addLink(uriInfo.getAbsolutePath().toString(), "self");
		message.addLink(getUriForProfile(uriInfo,message), "profile");
		message.addLink(getUriForComments(uriInfo,message), "comments");
		// String newId = String.valueOf(message.getId());
		// URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build();
		return Response.created(uriInfo.getAbsolutePath())
					   .entity(message)
					   .build();
	}

	private String getUriForProfile(UriInfo uriInfo, Message message) {
		URI uri = uriInfo.getBaseUriBuilder()
				 .path(ProfileResource.class)
				 .path(message.getAuthor())
				 .build();
       return uri.toString();
	}

	private String getUriForComments(UriInfo uriInfo, Message message) {
		URI uri = uriInfo.getBaseUriBuilder()
						 .path(MessageResource.class)
						 .path(MessageResource.class, "getCommentResource")
						 .path(CommentResource.class)
						 .resolveTemplate("messageId",message.getId())
						 .build();
		return uri.toString();
	}

	@POST
	@Path("/addmessage")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Message addMessage(Message message) {
		return messageService.addMessage(message);
	}

	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{messageId}")
	public Message updateMessage(@PathParam("messageId") long id, Message message) {
		message.setId(id);
		return messageService.updateMessage(message);
	}

	@DELETE
	@Path("/{messageId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void removeMessage(@PathParam("messageId") long id) {
		messageService.removeMessage(id);
	}
	

	
	@Path("/{messageId}/comments")
	public CommentResource getCommentResource() {
		return new CommentResource();
	}
	

}

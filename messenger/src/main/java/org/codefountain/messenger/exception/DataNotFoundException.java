package org.codefountain.messenger.exception;

public class DataNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 469868011526662169L;
	
	public DataNotFoundException(String message) {
		super(message);
	}

}
